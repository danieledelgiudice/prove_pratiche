#ifndef COMPITO_H
#define COMPITO_H

#include <iostream>

class Palazzo
{
	// Si memorizzano i piani in un unico array, creato "accostando"
	// tutti i piani uno accanto all'altro. L'accesso a caselle in posizione i,j
	// si ottiene con un paio di moltiplicazioni/somme.

	const int MAX_HEIGHT;
	const int SIZE;
	int height;
	bool* v;

	bool& idx(int, int);
	bool idx(int, int) const;

public:

	Palazzo(int);
	Palazzo(const Palazzo&);
	~Palazzo();

	bool aggiungi();
	bool cambia(int, int);

	Palazzo& operator%=(const Palazzo&);
	int operator!();

	friend std::ostream& operator<<(std::ostream&, const Palazzo&);
};

#endif