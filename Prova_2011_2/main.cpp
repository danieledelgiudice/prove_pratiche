#include "compito.h"
#include <iostream>

using namespace std;

int main(){
	//test costruttore/distruttore
	{
		Palazzo p(20);
		cout << p << endl;
	}

	Palazzo p1(5);
	cout << p1 << endl;

	//test costruttore di copia
	Palazzo p2(p1);
	cout << p2 << endl;

	//test aggiungi()
	p1.aggiungi();
	cout << p1 << endl;
	p1.aggiungi();
	cout << p1 << endl;


	//test cambia()
	p1.cambia(2, 1);
	cout << p1 << endl;
	p1.cambia(3, 3);
	cout << p1 << endl;


	//test operator!
	cout << !p1 << endl;

	//test operator%=
	Palazzo p3(5);
	p3.aggiungi();
	p3.aggiungi();
	cout << p3 << endl;
	p1 %= p3;
	cout << p1 << endl;


	system("PAUSE");

	return 0;
}
