#include <iostream>
#include "compito.h"

using namespace std;

bool& Palazzo::idx(int i, int j)
{
	return v[i*(i - 1) / 2 + j - 1];
}

bool Palazzo::idx(int i, int j) const
{
	return v[i*(i - 1) / 2 + j - 1];
}

Palazzo::Palazzo(int n) : MAX_HEIGHT(n),
SIZE(MAX_HEIGHT*(MAX_HEIGHT + 1) / 2)
{
	v = new bool[SIZE];

	for (int i = 0; i < SIZE; i++)
		v[i] = false;

	height = 1;
}

Palazzo::Palazzo(const Palazzo& p) : MAX_HEIGHT(p.MAX_HEIGHT),
SIZE(p.SIZE)
{
	v = new bool[SIZE];

	for (int i = 0; i < SIZE; i++)
		v[i] = p.v[i];

	height = p.height;
}

Palazzo::~Palazzo()
{
	delete[] v;
}

bool Palazzo::aggiungi()
{
	if (height == MAX_HEIGHT)
		return false;

	height++;

	return true;
}

bool Palazzo::cambia(int i, int j)
{
	if (i < 1 || j < 1 || i > height || j > i)
		return false;

	idx(i, j) = !idx(i, j);

	return true;
}

Palazzo& Palazzo::operator%=(const Palazzo& p)
{
	if (height != p.height)
		return *this;

	int size = height*(height + 1) / 2;

	for (int i = 0; i < size; i++)
	{
		if (!p.v[i])
			v[i] = false;
	}

	return *this;
}

int Palazzo::operator!()
{
	int c = 0;

	for (int i = 0; i < SIZE; i++)
	if (v[i])
		c++;

	return c;
}

ostream& operator<<(ostream& o, const Palazzo& p)
{
	o << '<' << p.height << '>' << endl;

	for (int i = 1; i <= p.height; i++)
	{
		o << "Piano " << i << ":";
		for (int j = 1; j <= i; j++)
			o << (p.idx(i, j) ? " Aperta" : " Chiusa");
		o << endl;
	}

	return o;
}
