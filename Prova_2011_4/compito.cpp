#include <iostream>
#include <cstdlib>
#include "compito.h"

using namespace std;

int Pavimento::pos_to_i(const Posizione& p)
{
	int x = p.lettera - 'A';
	int y = p.numero - 1;

	return x*SIZE + y;
}

Pavimento::Pavimento(int size=3) : SIZE(size)
{
	if (SIZE > 26)
	{
		cerr << "La dimensione del Pavimento non pu� superare 26." << endl;
		exit(-1);
	}

	v = new bool[SIZE*SIZE];

	for (int i = 0; i < SIZE*SIZE; i++)
		v[i] = false;
}

Pavimento::Pavimento(const Pavimento& p) : SIZE(p.SIZE)
{
	v = new bool[SIZE*SIZE];

	for (int i = 0; i < SIZE*SIZE; i++)
		v[i] = p.v[i];
}

Pavimento::~Pavimento()
{
	delete[] v;
}

bool Pavimento::aggiungi(const Pavimento::Posizione& p)
{
	int i = pos_to_i(p);

	if (v[i])
		return false;

	v[i] = true;
	return true;
}

bool Pavimento::togli(const Pavimento::Posizione& p)
{
	int i = pos_to_i(p);

	if (!v[i])
		return false;

	v[i] = false;
	return true;
}

bool Pavimento::sposta(const Pavimento::Posizione& p1, const Pavimento::Posizione& p2)
{
	int i = pos_to_i(p1);
	int j = pos_to_i(p2);

	if (!v[i] || v[j])
		return false;

	v[i] = false;
	v[j] = true;

	return true;
}

int Pavimento::operator!() const
{
	// Andrebbe sistemata, ma vabb� (cit)

	int max = 0;
	int count = 0;

	// Controllo righe
	for (int i = 0; i < SIZE; i++)
	{
		for (int j = 0; j < SIZE; j++)
		{
			if (v[i*SIZE + j])
				count++;
			else
			{
				if (max < count)
					max = count;
				count = 0;
			}
		}

		if (max < count)
			max = count;

		count = 0;
	}

	// Controllo colonne
	for (int j = 0; j < SIZE; j++)
	{
		for (int i = 0; i < SIZE; i++)
		{
			if (v[i*SIZE + j])
				count++;
			else
			{
				if (max < count)
					max = count;
				count = 0;
			}
		}

		if (max < count)
			max = count;

		count = 0;
	}
	
	return max;
}

int Pavimento::operator~() const
{
	int c = 0;

	for (int i = 0; i < SIZE*SIZE; i++)
	{
		if (v[i])
			c++;
	}

	return c;
}

ostream& operator<<(ostream& o, const Pavimento& p)
{
	for (int i = 0; i < p.SIZE*p.SIZE; i++)
	{
		o << (p.v[i] ? '0' : '-');

		if ((i + 1) % p.SIZE)
			o << '\t';
		else
			o << endl;
	}

	return o;
}