#include <iostream>
#include "compito.h"

using namespace std;

int main(){
	posizione pos, pos1, pos2, pos3, pos4, pos5, pos6;
	pos.lettera = 'C';
	pos.numero = 1;
	pos1.lettera = 'A';
	pos1.numero = 1;
	pos2.lettera = 'B';
	pos2.numero = 1;
	pos3.lettera = 'A';
	pos3.numero = 3;
	pos4.lettera = 'B';
	pos4.numero = 2;
	pos5.lettera = 'C';
	pos5.numero = 2;
	pos6.lettera = 'C';
	pos6.numero = 3;

	//test costruttore/distruttore
	{
		Pavimento p(5);
		cout << p << endl << endl;
	}

	Pavimento p1(3);
	cout << p1 << endl << endl;

	//test aggiungi()
	p1.aggiungi(pos);
	cout << p1 << endl << endl;
	p1.aggiungi(pos1);
	p1.aggiungi(pos2);
	p1.aggiungi(pos3);
	p1.aggiungi(pos4);
	p1.aggiungi(pos5);
	p1.aggiungi(pos6);
	cout << p1 << endl << endl;

	//test costruttore di copia
	Pavimento p2(p1);
	cout << p2 << endl << endl;


	//test togli() 
	p1.togli(pos6);
	cout << p1 << endl << endl;

	//test sposta()
	p1.sposta(pos5, pos6);
	cout << p1 << endl << endl;


	//test operator!
	cout << !p1 << endl << endl;

	//test operator~
	cout << ~p1 << endl << endl;



	system("PAUSE");

	return 0;
}
