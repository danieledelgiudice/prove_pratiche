#ifndef COMPITO_H
#define COMPITO_H

#include <iostream>

class Pavimento {
public:
	struct Posizione {
		char lettera;
		int numero;
	};

private:
	bool* v;
	const int SIZE;

	int pos_to_i(const Posizione&);

public:
	Pavimento();
	Pavimento(int);
	Pavimento(const Pavimento&);
	~Pavimento();

	bool aggiungi(const Posizione&);
	bool togli(const Posizione&);
	bool sposta(const Posizione&, const Posizione&);

	int operator!() const;
	int operator~() const;

	friend std::ostream& operator<<(std::ostream&, const Pavimento&);
};

// Mi rifiuto di chiamare una classe "posizione" e schiaffarla fuori da tutto,
// cambierei anche i nomi degli attributi se potessi
typedef Pavimento::Posizione posizione;

#endif