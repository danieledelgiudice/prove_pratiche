#include <iostream>
#include "compito.h"

using namespace std;

Palazzo::Palazzo(int n) : MAX_HEIGHT(n)
{
	v = new bool*[MAX_HEIGHT];

	v[0] = new bool[1];
	v[0][0] = false;

	height = 1;
}

Palazzo::Palazzo(const Palazzo& p) : MAX_HEIGHT(p.MAX_HEIGHT)
{
	v = new bool*[MAX_HEIGHT];

	height = p.height;

	for (int i = 0; i < height; i++)
	{
		v[i] = new bool[i + 1];
		for (int j = 0; j <= i; j++)
			v[i][j] = p.v[i][j];
	}

}

Palazzo::~Palazzo()
{
	for (int i = 0; i < height; i++)
		delete[] v[i];

	delete[] v;
}

bool Palazzo::aggiungi()
{
	if (height == MAX_HEIGHT)
		return false;

	v[height] = new bool[height + 1];
	for (int i = 0; i <= height; i++)
		v[height][i] = false;

	height++;

	return true;
}

bool Palazzo::cambia(int i, int j)
{
	if (i < 1 || j < 1 || i > height || j > i)
		return false;

	v[i - 1][j - 1] = !v[i - 1][j - 1];

	return true;
}

Palazzo& Palazzo::operator%=(const Palazzo& p)
{
	if (height != p.height)
		return *this;

	for (int i = 0; i < height; i++)
	for (int j = 0; j <= i; j++)
	{
		if (!p.v[i][j])
			v[i][j] = false;
	}

	return *this;
}

int Palazzo::operator!()
{
	int c = 0;

	for (int i = 0; i < height; i++)
	for (int j = 0; j <= i; j++)
	{
		if (v[i][j])
			c++;
	}

	return c;
}

ostream& operator<<(ostream& o, const Palazzo& p)
{
	o << '<' << p.height << '>' << endl;

	for (int i = 0; i < p.height; i++)
	{
		o << "Piano " << i + 1 << ":";
		for (int j = 0; j <= i; j++)
			o << (p.v[i][j] ? " Aperta" : " Chiusa");
		o << endl;
	}

	return o;
}
