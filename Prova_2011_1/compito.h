#ifndef COMPITO_H
#define COMPITO_H

#include<iostream>
#define PLATE_SIZE 7

class PassaggioALivello {

	/*
	Viene utilizzata una lista per memorizzare le auto in attesa
	di fronte al passaggio a livello. Il salvataggio delle auto
	avviene in modo "inverso". In fase di output si procederÓ
	a stampare nell'ordine corretto.
	*/

public:

	PassaggioALivello(char*);
	~PassaggioALivello();
	void cambiastato();
	void aggiungi(char*);

	int operator!() const;
	PassaggioALivello& operator-=(char*);

	friend std::ostream& operator<<(std::ostream&, const PassaggioALivello&);

	class NodoAuto {
		// Inner class per realizzare la lista
	public:
		NodoAuto* next;
		char s[PLATE_SIZE];

		NodoAuto(char*);

		friend std::ostream& operator<<(std::ostream&, const NodoAuto&);
	};

private:

	NodoAuto* head;
	int len;
	bool open;

	void clear();
};

#endif