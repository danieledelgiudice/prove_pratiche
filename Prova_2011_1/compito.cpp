#include "compito.h"
#include <iostream>

using namespace std;

/*
NODO AUTO
*/

PassaggioALivello::NodoAuto::NodoAuto(char* _s)
{
	strncpy(s, _s, PLATE_SIZE);
	next = NULL;
}

/*
PASSAGGIO A LIVELLO
*/

PassaggioALivello::PassaggioALivello(char* _s)
{
	NodoAuto* p = new NodoAuto(_s);
	head = p;

	open = false;
	len = 1;
}

PassaggioALivello::~PassaggioALivello()
{
	clear();
}

void PassaggioALivello::cambiastato()
{
	// Inverto lo stato, in ogni caso la lista diventa vuota
	open = !open;
	clear();
}

void PassaggioALivello::aggiungi(char* _s)
{
	if (!open)
	{
		// Inserimento in testa
		NodoAuto* p = new NodoAuto(_s);
		p->next = head;
		head = p;
		len++;
	}
}

void PassaggioALivello::clear()
{
	// Deleto tutta la lista
	NodoAuto* p = head;
	head = NULL;

	while (p != NULL)
	{
		NodoAuto* x = p->next;
		delete p;
		p = x;
	}
}

int PassaggioALivello::operator!() const
{
	// Non ho bisogno di contare la dimensione perch� me la salvo
	return len;
}

PassaggioALivello& PassaggioALivello::operator-=(char* _s)
{
	if (head != NULL)
	{
		// Controllo testa
		if (strcmp(head->s, _s) == 0)
			head = head->next;

		// Controllo nel mezzo
		NodoAuto* p = head;
		while (p->next != NULL)
		{
			if (!strcmp(p->next->s, _s))
			{
				// Cancello l'elemento, e aggiorno la dimensione
				NodoAuto* x = p->next;
				p->next = p->next->next;

				delete x;
				len--;

				break;
			}
		}
	}

	return *this;
}

ostream& operator<<(ostream& o, const PassaggioALivello& p)
{
	// Stampo stato del passaggio, e se la lista non � vuota anch'essa
	o << "Passaggio a livello: " << (p.open ? "APERTO" : "CHIUSO") << endl;

	if (p.head != NULL)
		o << *p.head;

	return o;
}

ostream& operator<<(ostream& o, const PassaggioALivello::NodoAuto& n)
{
	// @@@@@@ FUNZIONE RICORSIVA! @@@@@@
	//
	// Effettuo la ricorsione e dopo stampo l'elemento corrente
	// Il risultato � una stampa dal fondo verso l'inizio della lista
	// E' come se "srotolassi" la lista all'interno dello stack

	if (n.next != NULL)
		o << *n.next;

	o << n.s << endl;

	return o;
}
