#include <iostream>
#include "compito.h"

using namespace std;

int main(){
	//test costruttore/distruttore
	{
		PassaggioALivello p("XXXXXX");
	}
	PassaggioALivello p1("YY");
	cout << p1 << endl;

	//test operator!
	cout << !p1 << endl << endl;

	//test cambiastato()
	p1.cambiastato();
	cout << p1 << endl;

	//test aggiungi(s) con stato APERTO
	p1.aggiungi("LMNOO");
	cout << p1 << endl;

	//test aggiungi(s) con stato CHIUSO
	p1.cambiastato();
	p1.aggiungi("LMNOO");
	cout << p1 << endl;
	p1.aggiungi("LKK");
	p1.aggiungi("LO");
	cout << p1 << endl;

	//test operator-=(s)
	p1 -= "LKK";
	cout << p1 << endl;

	system("pause");
	return 0;
}
